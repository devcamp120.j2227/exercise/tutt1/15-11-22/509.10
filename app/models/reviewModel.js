//B1: khai báo thử viên mongoose
const mongoose = require('mongoose');

//B2: khai báo thư viện Schema của mongoose
const Schema = mongoose.Schema;

//B3: tạo đối tượng Schemal bao gồm các thuộc tính của collection trong mongodb
const reviewSchema = new Schema({
    _id : mongoose.Types.ObjectId,
    // _id : {
    //     type:mongoose.Types.ObjectId
    // }
    //stars : Number,
    stars : {
        type : Number,
        default : 0
    },
    note : {
        type : String,
        required : false
    },
    created_At : {
        type : Date,
        default : Date.now()
    },
    updated_At : {
        type : Date,
        default : Date.now()
    }    
});

//B4: export schemal ra model
module.exports = mongoose.model("review", reviewSchema);