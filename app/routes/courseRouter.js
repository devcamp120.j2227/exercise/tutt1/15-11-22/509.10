//khai báo thư viện express
const express = require('express');

//import controller
const { getAllCourses, getCourseById, createCourse, updateCourse, deleteCourse } = require('../controllers/courseController');

//tạo router
const courseRouter = express.Router();

//get all courses
courseRouter.get('/courses', getAllCourses);

//get a course
courseRouter.get('/courses/:courseid', getCourseById)

//create a course
courseRouter.post('/courses', createCourse);


//update a course
courseRouter.put('/courses/:courseid', updateCourse)

//delete a course
courseRouter.delete('/courses/:courseid', deleteCourse)

module.exports = { courseRouter };
