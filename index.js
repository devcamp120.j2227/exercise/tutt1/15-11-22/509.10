//khai báo thư viện express
const express = require('express');

//khai báo thư viện mongoose
const mongoose = require('mongoose');

//khai báo model mongoose
const reviewModel = require('./app/models/reviewModel');
const courseModel = require('./app/models/courseModel');

//khai báo router
const { courseRouter } = require('./app/routes/couresRouter');
const { reviewRouter } = require('./app/routes/reviewRouter');

//khởi tạo ứng dụng nodejs
const app = new express();

//sử dụng được body json
app.use(express.json());

//sử dụng body unicode
app.use(express.urlencoded({
    extended:true
}))

//khai báo port chạy nodejs
const port = 8000;

//kết nối mongodb
mongoose.connect('mongodb://localhost:27017/CRUD_Course', function(error) {
    if (error) throw error;
    console.log('Successfully connected to Mongodb !');
});
   
app.get('/', (request, response) => {
    let today = new Date();
    console.log(`Hôm nay là ngày ${today.getDate()} tháng ${today.getMonth() +1} năm ${today.getFullYear()}`);
    
    response.status(200).json({
        message: `Hôm nay là ngày ${today.getDate()} tháng ${today.getMonth() +1} năm ${today.getFullYear()}`
    })
})

//sử dụng router
app.use('/', courseRouter);
app.use('/', reviewRouter);

app.listen(port, () => {
    console.log(`App chạy trên cổng ${port}`);
})